import os
from csv_utils import convert_all_xml_files_to_csv
from video_utils import make_images_to_video, show_video_with_labeling

if __name__ == "__main__":
    current_directory = os.getcwd()
    images_directory = os.path.join(current_directory, "00_29_03", "images")
    annotations_directory = os.path.join(current_directory, "00_29_03", "annotations")
    convert_all_xml_files_to_csv(annotations_directory, "00_29_03.mp4")
    make_images_to_video(images_directory, "video.avi")
    try:
        show_video_with_labeling("video.avi", "output.csv")
    except ValueError as e:
        print(e)
