import os
import xml.etree.ElementTree as Xet
import pandas as pd
from typing import List


def get_middle_and_distance(coordinate_min: int, coordinate_max: int) -> tuple:
    """
    get middle and distance
    Args:
        coordinate_min: the x_min coordinate
        coordinate_max: the x_max coordinate

    Returns:
        the middle and distance
    """
    middle = (coordinate_min + coordinate_max) / 2
    distance = abs(coordinate_max) - abs(coordinate_min)
    return middle, distance


def extract_coordinates_from_xml_root(root: Xet) -> list:
    """
    Extract coordinates from xml root.
    Args:
         root: The xml root.
    Returns:
        The extracted coordinates in csv format.
    """

    xmin = int(root.find('bndbox').find('xmin').text)
    xmax = int(root.find('bndbox').find('xmax').text)
    ymin = int(root.find('bndbox').find('ymin').text)
    ymax = int(root.find('bndbox').find('ymax').text)

    middleX, distanceX = get_middle_and_distance(xmin, xmax)
    middleY, distanceY = get_middle_and_distance(ymin, ymax)

    return [middleX,
            middleY,
            distanceX,
            distanceY,
            None, None, None, None, None]


def convert_xml_to_row(xml_file_path: str, video_id, frame_number: str) -> List[dict]:
    """
    convert xml to row list
    Args:
        frame_number:frame number
        video_id: video id
        xml_file_path: xml file path

    Returns:
        list with all the row from xml file
    """
    detections = {}
    xml_annotation_file = Xet.parse(xml_file_path)
    root = xml_annotation_file.getroot()

    for object_tag in root.findall("object"):
        class_name = object_tag.find("class").text

        if not class_name in detections.keys():
            detections[class_name] = []

        detections[class_name].append(extract_coordinates_from_xml_root(object_tag))

    return {"video_id": video_id,
            "frame_id": str(frame_number),
            "homography": [],
            "detections": detections}


def convert_all_xml_files_to_csv(xml_directory_path: str, video_id: str):
    """
    convert all xml files in directory to csv
    Args:
        video_id: video id
        xml_directory_path: xml directory name
    """
    rows = []

    for index, xml_file in enumerate(os.listdir(xml_directory_path)):
        xml_file_annotation_path = os.path.join(xml_directory_path, xml_file)
        rows.append(convert_xml_to_row(xml_file_annotation_path, video_id, index))

    pd.DataFrame(rows).to_csv('output.csv', index=False)


def split_videos_csv_to_csv_for_each_video(videos_csv_name: str):
    """
    split videos csv to csv for each video
    Args:
        videos_csv_name: the csv file we want to split
    """
    videos_csv = pd.read_csv(videos_csv_name)

    for video_id, video_csv in videos_csv.groupby('video_id'):
        video_csv.to_csv(f'{video_id}.csv', index=False)


