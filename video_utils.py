import os
import cv2
import pandas as pd
import yaml


def make_images_to_video(images_directory_name: str, video_name: str):
    """
    make all image in specific directory to a video
    Args:
        images_directory_name: image directory name
        video_name: video name
    """
    images = [img for img in os.listdir(images_directory_name) if img.endswith(".png")]
    frame = cv2.imread(os.path.join(images_directory_name, images[0]))
    height, width, layers = frame.shape

    video = cv2.VideoWriter(video_name, 0, 1, (width, height))

    for image in sorted(images):
        video.write(cv2.imread(os.path.join(images_directory_name, image)))

    cv2.destroyAllWindows()
    video.release()


def show_video_with_labeling(video_name: str, video_csv: str):
    """
    show video with labeling
    Args:
        video_csv: name of csv file
        video_name: video name
    """
    color = (255, 0, 0)
    thickness = 2
    cap = cv2.VideoCapture(video_name)
    counter = 0

    if not cap.isOpened():
        raise ValueError("video cant open")

    while cap.isOpened():
        ret, frame = cap.read()

        if ret:
            annotation = pd.read_csv(video_csv)

            if annotation["frame_id"].loc[0] <= counter:
                for annotations in yaml.load(annotation["detections"].loc[counter]).values():
                    for annotation in annotations:
                        (cv2.rectangle(frame,
                                       (int(annotation[0] - (annotation[2] / 2)),
                                        int(annotation[1] - (annotation[3] / 2))),
                                       (int(annotation[0] + (annotation[2] / 2)),
                                        int(annotation[1] + (annotation[3] / 2))),
                                       color,
                                       thickness))
            cv2.imshow('Frame', frame)
            counter += 1
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break

        else:
            break

    cap.release()

    cv2.destroyAllWindows()
